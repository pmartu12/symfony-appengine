# symfony-appengine
A symfony / appengine running project with file upload on GCP Storage 

## Docker compose

First of all setup all services :
```
docker-compose up
```

If you modify the docker you must rebuild the container with this command :
```
docker-compose up --force-recreate --abort-on-container-exit --build
```

If you want to clear cache use this :
and then
```bash
docker-compose run php php composer.phar install && php bin/console assets:install public --no-interaction --no-ansi && php bin/console cache:clear
```

## Run command inside in docker container

```
docker exec -it <container-name> <command>
```
example :
```
docker exec -it <container-name> php bin/console d:s:u --dump-sql
```

## CODE

This application use the GCP PHP Lib to charge the file on Storage, to let it work make the following steps :

* Create a _Service Account_ with FULL STORAGE PERMISSION
* Copy the json key in the folder _/config/resources/google_cloud_service_account.json_
* Give to SA FULL permission on Storage GCP service
* Create .env.local with the correct env vars :
    * GCP_STORAGE_BUCKET_NAME : is the name of your bucket, where you want to upload your files.
    * GCP_STORAGE_EXPIRE : when generate temp links will expire
    
## Session

This application is stateful, because we use session but we are able to split PHP containers in more then one
because we will store our sessions in REDIS.

To be able to use REDIS we need to setup this env vars :
* REDIS_HOST='127.0.0.1'
* REDIS_PORT=6379

