#!/usr/bin/env sh
echo ">>>>>>>>> download composer.phar"
curl -sS https://getcomposer.org/installer | php
echo ">>>>>>>>> run composer"
php composer.phar install
echo ">>>>>>>>> remove composer.phar"
rm composer.phar
echo ">>>>>>>>> install symfony assets"
php bin/console assets:install public --no-interaction --no-ansi
echo ">>>>>>>>> cache clear of ${APP_ENV} env"
php bin/console cache:clear --env=${APP_ENV}
echo ">>>>>>>>> run fpm demon"
php-fpm
