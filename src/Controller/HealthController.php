<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HealthController extends AbstractController
{
    /**
     * @Route("/ping", name="ping")
     */
    public function ping()
    {
        return new Response('pong', 200);
    }

    /**
     * @Route("/health", name="health")
     *
     * @param EntityManagerInterface $entityManager
     *
     * @return JsonResponse
     */
    public function health(EntityManagerInterface $entityManager)
    {
        return new JsonResponse([
            'connection' => $entityManager->getConnection()->connect()
        ], 200);
    }
}
