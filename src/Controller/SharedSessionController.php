<?php


namespace App\Controller;


use DateInterval;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\RedisSessionHandler;
use Symfony\Component\Routing\Annotation\Route;

class SharedSessionController extends AbstractController
{
    /**
     * @Route("/session", name="file_manager")
     * @param Request $request
     * @param SessionInterface $session
     * @return Response
     */
    public function index2(Request $request, SessionInterface $session)
    {
        $form = $this->createFormBuilder([])
            ->add('name', TextType::class)
            ->add('value', TextType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $session->set($data['name'], $data['value']);
        }

        return $this->render('session/index.html.twig', [
            'sessionForm' => $form->createView(),
            'sessionAll' => $session->all()
        ]);
    }


    /**
     * @Route("/cache", name="cache")
     * @param AdapterInterface $cache
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function index(AdapterInterface $cache)
    {
        $cacheKey = 'thisIsACacheKey';
        $item = $cache->getItem($cacheKey);

        $itemCameFromCache = true;
        if (!$item->isHit()) {
            $itemCameFromCache = false;
            $item->set('this is some data to cache');
            $item->expiresAfter(new DateInterval('PT10S')); // the item will be cached for 10 seconds
            $cache->save($item);
        }

        return $this->render('cache/index.html.twig', ['isCached' => $itemCameFromCache ? 'true' : 'false']);
    }
}
