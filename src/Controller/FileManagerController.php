<?php

namespace App\Controller;

use App\Services\FileStorageService;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints\File;
use App\Model\PhotoProfile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FileManagerController extends AbstractController
{
    /**
     * @Route("/photo", name="file_manager")
     * @param Request $request
     * @param FileStorageService $fileStorageService
     * @return Response
     */
    public function index(Request $request, FileStorageService $fileStorageService)
    {
        $photoProfile = new PhotoProfile();

        $photoForm = $this->createFormBuilder($photoProfile)
            ->add('photo', FileType::class, [
                'label' => 'Photo',
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1024k'
                    ])
                ],
            ])->getForm();

        $request = Request::createFromGlobals();
        $photoForm->handleRequest($request);

        if ($photoForm->isSubmitted()) {
            if (!$photoForm->isValid()) {
                $this->addFlash('danger', 'The form is not valid.');
            } else {
                try {
                    /** @var UploadedFile $photoFile */
                    $photoFile = $photoForm->get('photo')->getData();
                    if ($photoFile) {
                        $originalFilename = pathinfo($photoFile->getClientOriginalName(), PATHINFO_FILENAME);
                        $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                        $newFilename = $safeFilename . '-' . uniqid() . '.' . $photoFile->guessExtension();
                        $fileStorageService->upload(fopen($photoFile->getRealPath(), 'rb'), $newFilename);
                        $this->addFlash('success', "Photo added to the bucket with name : $newFilename");
                    }
                } catch (\Exception $e) {
                    $this->addFlash('danger', 'Something went wrong with file storage upload. '.$e->getMessage());
                }
            }
        }
        return $this->render('file_manager/index.html.twig', [
            'photoForm' => $photoForm->createView()
        ]);
    }
}
