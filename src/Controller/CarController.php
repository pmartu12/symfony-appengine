<?php

namespace App\Controller;

use App\Entity\Car;
use App\Repository\CarRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CarController extends AbstractController
{
    /**
     * @Route("/car", name="car")
     * @param CarRepository $carRepository
     * @return Response
     */
    public function index(CarRepository $carRepository)
    {
        /** @var Car[] $cars */
        $cars = $carRepository->findAll();
        return $this->render('car/index.html.twig', [
            'cars' => $cars
        ]);
    }
}
