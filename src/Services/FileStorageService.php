<?php


namespace App\Services;


use DateTime;
use Google\Cloud\Storage\Bucket;
use Google\Cloud\Storage\StorageClient as GoogleStorageClient;
use Google\Cloud\Storage\StorageObject;
use Psr\Http\Message\StreamInterface;

class FileStorageService
{
    /** @var GoogleStorageClient */
    public $client;

    /** @var Bucket $bucket */
    public $bucket;

    /** @var DateTime */
    public $expires;

    /**
     * FileStorageService constructor.
     * @param $googleCloudServiceAccount
     * @throws \Exception
     */
    public function __construct($googleCloudServiceAccount)
    {
        /** @var GoogleStorageClient client */
        $this->client = new GoogleStorageClient([
            'keyFile' => $googleCloudServiceAccount,
        ]);

        /** @var Bucket bucket */
        $this->bucket = $this->client->bucket(getenv('GCP_STORAGE_BUCKET_NAME'));
        $this->expires = (new DateTime())->modify(getenv('GCP_STORAGE_EXPIRE'));
    }

    /**
     * @param $file
     * @param string $path
     * @return StorageObject
     */
    public function upload($file, string $path): StorageObject
    {
        /** @var StorageObject $object */
        return $this->bucket->upload($file, [
            'name'                => $path
        ]);
    }

    /**
     * @param string $path
     * @return StreamInterface
     */
    public function download(string $path): StreamInterface
    {
        /** @var StorageObject $object */
        $object = $this->bucket->object($path);
        return $object->downloadAsStream();
    }

    /**
     * @param string $path
     */
    public function delete(string $path): void
    {
        /** @var StorageObject $object */
        $object = $this->bucket->object($path);
        $object->delete();
    }
}
