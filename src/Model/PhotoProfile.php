<?php


namespace App\Model;


class PhotoProfile
{
    /** @var string $filename */
    private $filename;

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     * @return PhotoProfile
     */
    public function setFilename(string $filename): PhotoProfile
    {
        $this->filename = $filename;
        return $this;
    }
}
